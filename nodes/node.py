#! /usr/bin/env python
from skeleton_extrapolate import SkeletonExtrapolator
import rospy

if __name__ == '__main__':
    rospy.init_node("skeleton_extrapolate")
    extrapolator_node = SkeletonExtrapolator()
    extrapolator_node.run()
    extrapolator_node.close()