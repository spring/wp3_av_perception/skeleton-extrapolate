#!/usr/bin/env python3
import rospy
import numpy as np
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from spring_msgs.msg import Frame


class SkeletonExtrapolator:
    def __init__(self):
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)
        self.frame_topic="/frame"
        self.extrapolated_frame_topic="/frame_ext"
        self.CONFIDENCE_THRESHOLD=0.5
        self.CONFIDENCE_THRESHOLD_MIN=0.2
        self.idx_l_heel = 21  # 21/29 ## openpose/mediapipe
        self.idx_r_heel = 24  # 24/30  ## openpose/mediapipe
        self.idx_l_big_toe = 19  # 19/31  ## openpose/mediapipe
        self.idx_r_big_toe = 22  # 22/32  ## openpose/mediapipe
        self.idx_l_hip = 12
        self.idx_r_hip = 9
        self.idx_mid_hip = 8
        self.idx_neck = 1
        self.feet_indices = [self.idx_l_heel, self.idx_r_heel, self.idx_l_big_toe, self.idx_r_big_toe]
        self.minimum_indices = [self.idx_neck, self.idx_mid_hip]

# from http://villemin.gerard.online.fr/Biologie/CorpsPro_fichiers/image039.gif
        self.neck_height=0.818
        self.hip_height= 0.586
        self.torso_length = self.neck_height - self.hip_height
        self.hip_width = 0.191

# if hip is at image height hh and neck at nh, then : 
#  - height of the person (in pixel) is H=(hh-nh)/(torso_length)
#  - feet(floor) positions should be at image height fh= nh - H x neck_height
# we also use corrections factors because openpose doesn't detect true anatomic points
        self.CC1=-0.26
        self.CC2=1.7

        self.num_persons_pose_estimated = 0
        self.Extrapolate_skeleton_count = 0

        self._publishers = []
        self._subscribers = []
        self._timers = []


    def frame_callback(self,data):
        self.num_persons_pose_estimated = len(data.persons)
        self.Extrapolate_skeleton_count = 0
        if self.num_persons_pose_estimated > 0 :
            #joints_data = np.zeros((num_persons_pose_estimated, nb_joints, 2))

            for person_idx, person in enumerate(data.persons):
                #for body_part_idx, body_part in enumerate(person.bodyParts):
                #joints_data[person_idx, body_part_idx, :] = [body_part.pixel.x, body_part.pixel.y]
                if np.any([person.bodyParts[i].score < self.CONFIDENCE_THRESHOLD for i in self.feet_indices]):
                # something is wrong with the skeleton's feets, we'll try to reconstruct them
                # provided we have a hip and a neck
                    if np.any([person.bodyParts[i].score < self.CONFIDENCE_THRESHOLD_MIN for i in self.minimum_indices]):
                        print("really bad skeleton ",person.bodyParts[self.idx_neck].score, " ", person.bodyParts[self.idx_mid_hip].score )
                    else :
                        self.Extrapolate_skeleton_count += 1
                        person_height=(person.bodyParts[self.idx_mid_hip].pixel.y-person.bodyParts[self.idx_neck].pixel.y)/self.torso_length 
                        nh = person.bodyParts[self.idx_neck].pixel.y             
                        feet= nh + person_height * self.neck_height + self.CC1*person_height
                    
                        data.persons[person_idx].bodyParts[self.idx_l_heel].pixel.x=person.bodyParts[self.idx_mid_hip].pixel.x
                        data.persons[person_idx].bodyParts[self.idx_l_heel].pixel.y=feet
                        data.persons[person_idx].bodyParts[self.idx_l_heel].score=0.6
                        data.persons[person_idx].bodyParts[self.idx_r_heel].pixel.x=person.bodyParts[self.idx_mid_hip].pixel.x
                        data.persons[person_idx].bodyParts[self.idx_r_heel].pixel.y=feet
                        data.persons[person_idx].bodyParts[self.idx_r_heel].score=0.6
                    # for the toes we'll need an estimation of the body orientation, and an arbitrary feet length
                    
                        normal_hip_width=self.hip_width * person_height 
                        measured_hip_width=person.bodyParts[self.idx_l_hip].pixel.x - person.bodyParts[self.idx_r_hip].pixel.x 
                        hip_ratio=max(min(measured_hip_width*self.CC2/normal_hip_width,1.0),-1) 
                    # unfortunately, hip_ratio sill always be in [0,1], so theta will be in [0,pi/2]
                    # i could not figure out a way of get the correct sign for tetha
                        tetha=np.arccos(hip_ratio)
                        print("estimated feet position ", person.bodyParts[self.idx_mid_hip].pixel.x,",",feet, " hip width ratio : ",
                          hip_ratio, " hip angle ", tetha) 
                        f_length=20.0
                        data.persons[person_idx].bodyParts[self.idx_l_big_toe].pixel.x=person.bodyParts[self.idx_mid_hip].pixel.x + f_length * np.sin(tetha)
                        data.persons[person_idx].bodyParts[self.idx_l_big_toe].pixel.y=feet + f_length * np.cos(tetha)
                        data.persons[person_idx].bodyParts[self.idx_l_big_toe].score=0.6
                        data.persons[person_idx].bodyParts[self.idx_r_big_toe].pixel.x=person.bodyParts[self.idx_mid_hip].pixel.x + f_length * np.sin(tetha)
                        data.persons[person_idx].bodyParts[self.idx_r_big_toe].pixel.y=feet + f_length * np.cos(tetha)
                        data.persons[person_idx].bodyParts[self.idx_r_big_toe].score=0.6
                    # if we have  left and right hip, then we use them for the feets horizontal coordinates
                        if ((person.bodyParts[self.idx_l_hip].score >= self.CONFIDENCE_THRESHOLD_MIN) 
                            and (person.bodyParts[self.idx_r_hip].score >= self.CONFIDENCE_THRESHOLD_MIN)) :
                            data.persons[person_idx].bodyParts[self.idx_l_heel].pixel.x=person.bodyParts[self.idx_l_hip].pixel.x
                            data.persons[person_idx].bodyParts[self.idx_r_heel].pixel.x=person.bodyParts[self.idx_r_hip].pixel.x
                            data.persons[person_idx].bodyParts[self.idx_l_big_toe].pixel.x=person.bodyParts[self.idx_l_hip].pixel.x +  f_length * np.sin(tetha)
                            data.persons[person_idx].bodyParts[self.idx_r_big_toe].pixel.x=person.bodyParts[self.idx_r_hip].pixel.x + f_length * np.sin(tetha)
                else :
                # squeleton quality is good, we can republish it without modification
                # the following if for evaluation purposes only : comparing the actual feet position with it's guess
                    person_height=(person.bodyParts[self.idx_mid_hip].pixel.y-person.bodyParts[self.idx_neck].pixel.y)/self.torso_length 
                    nh = person.bodyParts[self.idx_neck].pixel.y
                # feet prediction             
                    feet= nh + person_height * self.neck_height + self.CC1 * person_height
                # real feet position and error
                    hh=(person.bodyParts[self.idx_l_heel].pixel.y+person.bodyParts[self.idx_l_heel].pixel.y)/2.0
        #republish the data
        self.pub.publish(data)


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: AV Perception: Skeleton Extrapolate'
        status.values.append(KeyValue(key='Number of detected skeletons with Openpose', value='{}'.format(self.num_persons_pose_estimated)))
        status.values.append(KeyValue(key='Number of extrapolated skeletons', value='{}'.format(self.Extrapolate_skeleton_count)))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if self.num_persons_pose_estimated == 0:
            status.level = DiagnosticStatus.OK
            status.message = 'No skeleton detected with Openpose in the front fisheye 2D plane as input'

        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)


    def run(self):
        rospy.init_node('skeleton_extrapolate')
        self.pub = rospy.Publisher(self.extrapolated_frame_topic, Frame, queue_size=1)
        self._publishers.append(self.pub)
        self._subscribers.append(rospy.Subscriber(self.frame_topic, Frame, callback=self.frame_callback, queue_size=1))
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
        self._publishers.append(self._diagnostics_pub)
        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))
        rospy.spin()

    
    def close(self):
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()


if __name__ == '__main__':
    extrapolator=SkeletonExtrapolator()
    extrapolator.run()

