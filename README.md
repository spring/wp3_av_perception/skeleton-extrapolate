# skeleton-extrapolate



## purpose

This modules check openpose's output '''/frame''' topic and look for incomplete skeletons (without feet) that cannot be processed by the front_fisheye_2d_body_pose_detector module. It tries to extrapolate feets to the skeleton using the neck-hip lenght and hip width.

## input / output

Publications:
 * /frame_ext [spring_msgs/Frame]

Subscriptions:
 * /frame [spring_msgs/Frame]
